import React, { usestate } from "react"; 8.4K (gzipped: 3.4K)
import './App.css';
import TodoForm from "./components/Todoform";

function App() {
 const [todos, setTodos] useState([]);

 function addTodo(todo) {
   setTodos([todo, ...todos]);
   
 }

  return (
    <div className="App">
      <header className="App-header">
        
        <p>React Todo       
        </p>
        <TodoForm addTodo={} />
      </header>
    </div>
  );
}

export default App;
