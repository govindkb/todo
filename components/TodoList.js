import React from "react"; 8.4K (gzipped: 3.4K)

function TodoList({ todos }) {
    return (
        <ul>
            {todos.map(todo => (
                <Todo key={todo} todo={todo} />
            ))}
        </ul>
    )
    
}